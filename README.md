Xebia Studio Website
--------------------
This repository contains the source code of the Xebia Studio Website.
It is served by Jekyll, with a little bit of Liquid templating and some JavaScript.

* Install [jekyll](https://github.com/mojombo/jekyll/wiki/install)
* Clone the repository
* Run jekyll --server
* Open http://localhost:4000 in your browser.
* If you change code, it is immediately available in your browser (after a page refresh).

Contribute
----------
Fork the project and clone your fork locally. After that add the upstream:

     git remote add upstream git://github.com/xebia/xebia-studio-website.git

Create your feature branch

    git checkout -b what_I_want_to_work_on

Hack, commit, hack, HACK, commit !
Push your changed branch to your clone

    git push origin what_I_want_to_work_on:what_I_want_to_work_on

Go to GitHub, select the branch `what_I_want_to_work_on` and make a pull request. If you fix a known issue, consider adding `fixes #xxxx` or `closes #xxx` to your commit comment, so your fix is automatically added to the issue.

Make sure your changes are W3C compliant, if you hack Liquid or add new JS/CSS, please tell us in the pullrequest.

Links
-----
* [http://xebia.github.com/xebia-studio-website/](http://xebia.github.com/xebia-studio-website/)
* [http://studio.xebia.fr/](http://studio.xebia.fr/)
